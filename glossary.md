## Glossary

### 번역 완료된 단어

| 번역 소스 | 한국어 번역 | 노트 |
| ----- | ----- | ----- |
| Issue(s) | 이슈 |  |
| Merge request | 머지 리퀘스트 |  |
| repository | 리포지토리 |  |
| Branches | 브랜치 |   |
| feature | 기능 |   |
| review app | 리뷰 앱 | 제품 이름 - [문서](https://docs.gitlab.com/ee/ci/review_apps/index.html) |
| pipelines | 파이프라인 |   |
| cluster | 클러스터 |   |
| container registry | 컨테이너 레지스트리 |   |
| account | 계정 |   |
| label | 라벨 |   |
| key | 키 |   |
| circuitbreaker | 서킷 브레이커 |   |
| issue boards | 이슈 보드 |   |
| documentation | 문서 |   |
| projects | 프로젝트 |   |
| group | 그룹 |   |
| filter | 필터 |   |
| instance | 인스턴스 |   |
| read-only | 읽기 전용 |   |
| token | 토큰 |   |
| emoji | 이모지 |   |
| section | 섹션 |   |
| path | 경로 |   |
| change | 변경 |   |
| password | 비밀번호 |   |
| indexing / index | 인덱싱 | #15 |
| seat(s) | 좌석 | #21, 주로 라이선스 관련 사용자 정보 관련 |
| member(s) | 멤버 | #22 구성원, 회원 등 혼용되었으나 멤버로 통일 |
| override | 오버라이드 | #11 음차 버전을 사용합니다. |
| compliance | 컴플라이언스 | 음차 버전을 사용합니다. |
| delivery | 딜리버리 | 음차 버전을 사용합니다. |
| detection | 디텍션 | 음차 버전을 사용합니다. |
| fuzz testing | fuzz 테스트 | 음차 버전을 사용합니다. |
| dependency | 의존성 | 종속성 대신 의존성으로 통일 |


- 🚧 업데이트 중 입니다. 🚧

### 번역 시 주의가 필요한 단어

| 번역 소스 | 한국어 번역 | 노트 |
| ----- | ----- | ----- |
| annotation | 주석 | 협의 사항 - #4  |
| comment | 댓글, 주석 | 대부분은 댓글로 번역하지만, 코드 내부의 comment는 주석으로 번역합니다. |
| link | 링크 | 예시 - linking: 링크하기, linked: 링크된 / 협의 사항 - #2 |
| you | 당신 | 협의 사항 - #6 |
| pending | 대기 중, 제출 | 경우에 따라 **제출**로 번역합니다. 예시 - Summarize my pending comments - 내가 제출한 댓글, 협의 사항 - #3 |

### 협의가 필요한 단어

| 번역 소스 | 한국어 번역 | 노트 |
| ----- | ----- | ----- |
| plan | <Billing> 요금제 | 그냥 플랜도 괜찮을 듯 |
| Plan | <Navigation> 계획 | 그냥 플랜도 괜찮을 듯 | 
| tier | <Billing> 티어 | 티어 이름은 영어로 그대로 적습니다. 예시 - Free 티어, Premium 티어, Ultimate 티어 |
| compute minutes | 컴퓨팅 시간(분) | 
| compute unit | 컴퓨팅 단위 | 
